from django.conf import settings
from django.core.management import call_command
from django.test import TestCase
import os.path

from employee.models import Employee
from roster.models import Shift

class ShiftAllocationTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        def path(filename):
            return os.path.join(settings.BASE_DIR, 'files', filename)
        call_command('import_employees', path('employees.csv'))
        call_command('import_shifts', path('shifts.csv'))
        cls.e = Employee.objects.get(id=4)

    def test_empty_shift_allocation(self):
        self.assertEqual(self.e.shifts.count(), 0)

    def test_adjacent_shifts(self):
        shifts = Shift.objects.all()
        self.e.shifts.add(shifts[1])
        self.assertEqual(self.e.shifts.count(), 1)
        self.assertEqual(self.e.can_work_shift(shifts[0]), False)
        self.assertEqual(self.e.can_work_shift(shifts[2]), False)
        self.assertEqual(self.e.can_work_shift(shifts[3]), True)
        self.assertEqual(self.e.can_work_shift(shifts.last()), True)
