"""
urlconf for rostering project.

For more information and examples, see 
https://docs.djangoproject.com/en/2.1/topics/http/urls/#example
"""

from django.conf import settings
from django.contrib import admin
from django.urls import include, path
from rest_framework import routers

from employee.views import EmployeeViewSet
from roster.views import ShiftViewSet

router = routers.DefaultRouter()
router.register('employees', EmployeeViewSet)
router.register('shifts', ShiftViewSet)

urlpatterns = [
    path('api/', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('admin/', admin.site.urls),
]

if settings.DEBUG:
    if 'debug_toolbar' in settings.INSTALLED_APPS:
        urlpatterns.append(
            path('__debug__/', include('debug_toolbar.toolbar', namespace='djdt')),
        )

    # Serve media files in development. Note Django automatically serves
    # static files as the staticfiles app is active in settings.py.
    from django.conf.urls.static import static
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
