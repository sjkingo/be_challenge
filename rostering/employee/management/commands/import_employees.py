from django.core.management.base import BaseCommand
import csv

from employee.models import Employee

class Command(BaseCommand):
    help = 'Imports employees from CSV'

    def add_arguments(self, parser):
        parser.add_argument('csv_filename')

    def handle(self, *args, **options):
        Employee.objects.all().delete()
        with open(options['csv_filename'], 'rU', encoding='utf-8-sig') as fp:
            for row in csv.DictReader(fp):
                employee = Employee(
                    first_name=row['First Name'],
                    last_name=row['Last Name'],
                )
                employee.save()
                print('Created', employee)
