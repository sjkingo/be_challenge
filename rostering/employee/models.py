from django.db import models

class Employee(models.Model):
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)

    class Meta:
        ordering = ['first_name', 'last_name']

    def __str__(self):
        return f'{self.first_name} {self.last_name}'

    def can_work_shift(self, desired_shift):
        """
        Given a Shift object, return True if the employee can work the shift,
        that is, they do not have any overlapping or adjacent shifts.
        """
        overlaps = []
        for existing_shift in self.shifts.all():
            overlaps += list(existing_shift.overlaps)
        return desired_shift not in overlaps
