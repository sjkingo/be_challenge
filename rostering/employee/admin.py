from django.contrib import admin

from .models import Employee

class EmployeeAdmin(admin.ModelAdmin):
    list_display = ['__str__', 'first_name', 'last_name', 'num_shifts']

    def num_shifts(self, obj):
        return obj.shifts.count()

admin.site.register(Employee, EmployeeAdmin)
