from django import forms
from django.contrib import admin

from .models import Shift

class ShiftForm(forms.ModelForm):
    class Meta:
        model = Shift
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # Filter the list of available employees
        Employee = self.fields['employees']._queryset.model
        valid_employee_ids = [e.id for e in Employee.objects.all() if e.can_work_shift(kwargs['instance'])]
        self.fields['employees'] = forms.ModelMultipleChoiceField(
            queryset=Employee.objects.filter(id__in=valid_employee_ids),
            required=self.fields['employees'].required,
            widget=admin.widgets.FilteredSelectMultiple('Employees', is_stacked=False),
        )

class ShiftAdmin(admin.ModelAdmin):
    form = ShiftForm
    list_display = ['__str__', 'datetime_start', 'datetime_end', 'break_mins', 'num_employees']

    def num_employees(self, obj):
        return obj.employees.count()

admin.site.register(Shift, ShiftAdmin)
