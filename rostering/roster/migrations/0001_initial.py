# Generated by Django 2.1.4 on 2018-12-18 07:20

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('employee', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Shift',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('datetime_start', models.DateTimeField()),
                ('datetime_end', models.DateTimeField()),
                ('break_mins', models.PositiveSmallIntegerField(verbose_name='Break (mins)')),
                ('employees', models.ManyToManyField(blank=True, related_name='shifts', to='employee.Employee')),
            ],
            options={
                'ordering': ['datetime_start', 'datetime_end'],
            },
        ),
        migrations.AlterUniqueTogether(
            name='shift',
            unique_together={('datetime_start', 'datetime_end')},
        ),
    ]
