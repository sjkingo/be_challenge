from rest_framework import viewsets

from .models import Shift
from .serializers import ShiftSerializer

class ShiftViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows shifts to be viewed or edited.
    """

    queryset = Shift.objects.all()
    serializer_class = ShiftSerializer
