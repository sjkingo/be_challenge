from django.core.management.base import BaseCommand
from pprint import pprint

from employee.models import Employee
from roster.models import Shift

class Command(BaseCommand):
    help = 'Allocates employees to shifts'

    def handle(self, *args, **options):
        shifts = Shift.objects.all()
        max_shifts = shifts.count()
        shift_num = 0

        skips = {}

        for i in range(5):
            print('Allocation round', i)

            for employee in Employee.objects.all():
                if i == 0:
                    employee.shifts.clear()

                if shift_num + 1 > max_shifts:
                    shift_num = 0
                shift = shifts[shift_num]
                shift_num += 1

                if not employee.can_work_shift(shift):
                    skips.setdefault(employee, [])
                    skips[employee].append(shift)
                    continue

                employee.shifts.add(shift)
                print(employee, '->', shift)

            print()

        print('Skipped shifts:')
        pprint(skips)
