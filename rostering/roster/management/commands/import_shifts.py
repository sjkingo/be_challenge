import datetime
from dateutil.parser import parse
from django.core.management.base import BaseCommand
import csv

from roster.models import Shift

class Command(BaseCommand):
    help = 'Imports shifts from CSV'

    def add_arguments(self, parser):
        parser.add_argument('csv_filename')

    def handle(self, *args, **options):
        Shift.objects.all().delete()
        with open(options['csv_filename'], 'rU', encoding='utf-8-sig') as fp:
            for row in csv.DictReader(fp):
                start_date = parse(row['Date']).date()
                end_date = start_date

                start_time = parse(row['Start']).time()
                end_time = parse(row['End']).time()

                if end_time < start_time:
                    # end time is on next day
                    end_date = end_date + datetime.timedelta(days=1)

                start = datetime.datetime.combine(start_date, start_time)
                end = datetime.datetime.combine(end_date, end_time)

                shift, created = Shift.objects.get_or_create(
                    datetime_start=start,
                    datetime_end=end,
                    defaults={
                        'break_mins': int(row['Break']),
                    }
                )
                if created:
                    print('Created', shift)
