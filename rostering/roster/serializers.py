from rest_framework import serializers

from .models import Shift

class ShiftSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Shift
        fields = ['url', 'datetime_start', 'datetime_end', 'break_mins', 'employees']
