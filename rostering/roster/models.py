from django.db import models
from django.db.models import Q

from employee.models import Employee

class ShiftManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().prefetch_related('employees')

class Shift(models.Model):
    datetime_start = models.DateTimeField()
    datetime_end = models.DateTimeField()
    break_mins = models.PositiveSmallIntegerField('Break (mins)')
    employees = models.ManyToManyField(Employee, blank=True, related_name='shifts')

    objects = ShiftManager()

    class Meta:
        ordering = ['datetime_start', 'datetime_end']
        unique_together = ('datetime_start', 'datetime_end')

    def __str__(self):
        return f'Shift: {self.datetime_start} to {self.datetime_end}'

    @property
    def overlaps(self):
        """
        Returns any shifts that overlap or are adjacent with this one.
        """
        return Shift.objects.exclude(id=self.id).filter(
            Q(datetime_start__lte=self.datetime_start, datetime_end__gte=self.datetime_start) |
            Q(datetime_start__lt=self.datetime_end, datetime_end__gte=self.datetime_end)
        )
