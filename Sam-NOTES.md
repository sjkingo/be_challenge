# Sam's BE_Challenge Notes

Total time taken to complete: ~2.5 hours

## Overview of system

* Python 3.7
* Django 2.1 with DRF
* PostgreSQL database
* Fully working Django project
* Tests for algorithm

## Employees and Shifts

The rostering system is made up of two models: `Employee` and `Shift`. These
mimic the sample data provided in the `files` directory. An assumption was
made that each shift is distinct (there are no duplicates), and multiple
employees may be assigned to one shift. This means that although there are
duplicate shifts in the CSV file, they are represented as one object in the
`Shift` model.

Generally speaking, this means there are 3 shifts per day, with the overnight
shift (9.30 pm to 5.30 am) starting and finishing on different days.

An employee may be assigned to any shift, provided that this operation does
not mean they are working overlapping or adjacent shifts. In practice this
means that there will always be a gap period between their shifts.

## Restricting shift selection in admin

The Django admin panel has been tweaked slightly in `rostering/roster/admin.py`
to only allow the selection of employees for a shift that meet the above criteria.

For example, assuming shifts are consecutively numbered:

* Employee A is assigned to Shift 1 in the admin.

* Employee A will not appear in the available employee list for Shift 2 in the
  admin, as they are ineligible since Shift 1 and 2 are adjacent.

Note the API has no such restriction.

## Sample data import

Two management commands exist for parsing and importing the provided CSV
files in the admin. They can be called as follows:

    python manage.py import_employees <path_to_csv>

    python manage.py import_shifts <path_to_csv>

Please note the `import_shifts` command is destructive: it will delete all
existing shifts first, which removes any Employee <-> Shift relationships.

## API - DRF

DRF is installed and configured with read and writable endpoints for both the
`Employee` and `Shift` models. For the purposes of this prototype, no
authentication is required. It is accessible at http://localhost:8000/api/

In this prototype, no restriction on assigning employees to shifts exists in the API.

## Implementation of allocation algorithm

A management command exists for automatically allocating employees to shifts.
It adheres to the conditions set out above in terms of adjacent shifts and attempts
to allocate in an even distribution of shifts.

It can be called as follows:

    python manage.py allocate_shifts

The algorithm works like so:

1. The list of employees is iterated over in alphabetical order and one
   employee is assigned per increasing shift (e.g. Employee A to Shift 1,
   Employee B to Shift 2 etc).

2. When the list of employees or shifts is exhausted (whichever comes first),
   that list is repeated (e.g. Employee A to Shift 8), provided there is at
   least 1 shift in between the desired, previous and next shifts.

3. This process is repeated 5 times.

A list of shifts that were skipped due to the conditions is printed at the
end. Unless shifts have been manually assigned, there should be no skipped
shifts by running the allocation script.

## Tests

A minimal test case exists to verify the calculation of adjacent and
overlapping shifts works correctly.

It can be run with the usual Django test runner:

    python manage.py test

## Further work and questions

1. A test case be developed to verify the allocation algorithm does not violate
   the conditions for shift allocation.

2. The Shift API viewset should be extended to prevent assigning employees to shifts
   they cannot work on (adjacency, etc).
